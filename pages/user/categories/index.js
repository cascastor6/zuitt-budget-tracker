    import { useState, useEffect } from 'react'
    import { Table, Button } from 'react-bootstrap'
    import Link from 'next/link'
    import View from '../../../components/View'
    import AppHelper from '../../../app-helper'

    export default () => {
    
        const [categories, setCategories] = useState([])

        useEffect(() => {
            //const token = window.localStorage.getItem('token')
            const token = AppHelper.getAccessToken()
            fetch(`https://intense-fortress-06200.herokuapp.com/api/users/get-categories`, {
            //fetch(`http://localhost:4000/api/users/get-categories`, {
                method: 'POST',
                headers: {
                    'Content-Type':'application/json',
                    //'Authorization':`Bearer ${AppHelper.getAccessToken()}`
                    'Authorization':`Bearer ${token}`
                },
                //body: JSON.stringify({userId: `${AppHelper.getAccessToken().id}`})
            })
            .then(res => res.json())
            .then(data => {
                console.log(data)
                setCategories(data)
            })
            
        })

        return (
            
            <View title="Categories">
                <h3>Categories</h3>
                <Link href="/user/categories/new"><a className="btn btn-success mt-1 mb-3">Add</a></Link>
                <Table striped bordered hover variant="secondary">
                    <thead>
                        <tr>
                            <th>Category</th>
                            <th>Type</th>
                        </tr>
                    </thead>
                    <tbody>
                    {  
                    categories.map((category) => {
                            return (
                                <tr>
                                <td>{category.name}</td>
                                <td>{category.type}</td>
                                </tr>
                            )
                        })
                    }
                    </tbody>
                </Table>
            </View>
        )
    }
